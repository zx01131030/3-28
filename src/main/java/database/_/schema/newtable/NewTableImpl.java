package database._.schema.newtable;

import database._.schema.newtable.generated.GeneratedNewTableImpl;

/**
 * The default implementation of the {@link
 * database._.schema.newtable.NewTable}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class NewTableImpl 
extends GeneratedNewTableImpl 
implements NewTable {}