package database._.schema.newtable;

import database._.schema.newtable.generated.GeneratedNewTable;

/**
 * The main interface for entities of the {@code NewTable}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface NewTable extends GeneratedNewTable {}