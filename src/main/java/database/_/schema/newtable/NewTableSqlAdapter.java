package database._.schema.newtable;

import database._.schema.newtable.generated.GeneratedNewTableSqlAdapter;

/**
 * The SqlAdapter for every {@link database._.schema.newtable.NewTable} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class NewTableSqlAdapter extends GeneratedNewTableSqlAdapter {}